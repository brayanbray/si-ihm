<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administrateur extends CI_Model
{
    function __construct(){
        parent::__construct();
    }
	
    function testSession(){
		if(isset($sess))
			$this->load->view('back-office-index');
		else
			$this->load->view('back-office');	
    }

    function testConnexion(){
		if(isset($_POST['login']) && isset($_POST['mdp'])){
			$login=$_POST['login']; $mdp=$_POST['mdp'];
			$sql='SELECT id, login,mdp FROM administrateur WHERE login="'.$login.'" and mdp=sha1("'.$mdp.'")';
			$query=$this->db->query($sql);
			$row=$query->row_array();
			if(empty($row)) redirect(site_url('BackOffice?error=0'));
			else{
				session_start();
				$this->session->set_userdata('backOffice',$row);
				$_SESSION['backOffice']=$row;
				$this->load->view('back-office-index');
			} 
		}
			// foreach ($query->result_array() as $row){
			// 	echo $row['id'];
			// 	echo $row['login'];
			// }
    }
}

?>
