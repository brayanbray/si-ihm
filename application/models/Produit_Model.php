<?php defined('BASEPATH') OR exit('No direct script access allowed');
    class Produit_Model extends CI_Model{
        public function getProduct($idCategory=0){
            $rs ='';
            if($idCategory==0){
                $rs = $this->db->query("select * from produit");
            }
            else{
                $sql = sprintf("select * from produit where categorie=%s",$idCategory);
                $rs = $this->db->query($sql);
            }
            $result = array();
            foreach($rs->result_array() as $row){
                array_push($result,$row);
            }
            return $result;
        }
    }
?>