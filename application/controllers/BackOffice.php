<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BackOffice extends CI_Controller {
	public function index()
	{
		$this->load->view('back-office');	
		$this->load->model('administrateur');	
		$this->administrateur->testSession();
	}		

	public function traitement(){
		$this->load->model('administrateur');
		$this->administrateur->testConnexion();
	}
}
