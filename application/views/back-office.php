<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Administrateur</title>
        <meta charset="UTF-8"> 
        <link rel="stylesheet" href="<?php echo site_url('assets/css/all.min.css');?>">
		<link rel="stylesheet" href="<?php echo site_url('assets/css/bootstrap.min.css');?>">
    </head>
    <body class="text-center" style="background-size:cover;  background-image:url(<?php echo site_url('assets/bg.jpg');?>">
        <nav class="navbar navbar-expand-lg navbar-white"  style="color:white; background:rgba(0,0,0,.1)">
        </nav>
        <div style="margin-top:-10%; color:#E8E3FD;margin-top:10%; border:1px solid grey; background:rgba(0,0,0,.07);
        border-radius:20px; padding-bottom:2%; padding-top:2%"  class='offset-6 col-md-4'>
            <form class="form-signin" method="post" action="<?php echo site_url('BackOffice/traitement/submit'); ?>">
                <h1 class="h3 mb-3 font-weight-normal" style="white">Connexion</h1>
                <input type="text" class="form-control" name="login" placeholder="Adresse électronique" required>
                <input style="margin-top: 2%" type="password" name="mdp" class="form-control" placeholder="Mot de passe" required>
                <?php if(isset($_GET['error'])){ ?>
                    <div class="alert alert-danger" style="margin-top: 2%" role="alert">
                        Erreur! Réessayer s'il vous plait!
                    </div>
                <?php } ?>		
                <button style="margin-top: 2%" class="btn btn-primary btn-block" type="submit">Se connecter</button>
            </form>
        </div>
    </body>
</html>