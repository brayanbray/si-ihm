
  <!-- Footer -->
  <footer class="page-footer text-center text-md-left stylish-color-dark pt-0">

    <div style="background-color: #4285f4;">

      <div class="container">

        <!-- Grid row -->
        <div class="row py-4 d-flex align-items-center">

          <!-- Grid column -->
          <div class="col-md-6 col-lg-5 text-center text-md-left mb-4 mb-md-0">

            <h6 class="mb-0 white-text">Get connected with us on social networks!</h6>

          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6 col-lg-7 text-center text-md-right">

            <!-- Facebook -->
            <a class="fb-ic ml-0 px-2">

              <i class="fab fa-facebook-f white-text"> </i>

            </a>

            <!-- Twitter -->
            <a class="tw-ic px-2">

              <i class="fab fa-twitter white-text"> </i>

            </a>

            <!-- Google + -->
            <a class="gplus-ic px-2">

              <i class="fab fa-google-plus-g white-text"> </i>

            </a>

            <!-- Linkedin -->
            <a class="li-ic px-2">

              <i class="fab fa-linkedin-in white-text"> </i>

            </a>

            <!-- Instagram -->
            <a class="ins-ic px-2">

              <i class="fab fa-instagram white-text"> </i>

            </a>

          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>

    </div>

    <!-- Footer Links -->
    <div class="container mt-5 mb-4 text-center text-md-left">

      <div class="row mt-3">

        <!-- First column -->
        <div class="col-md-3 col-lg-4 col-xl-3 mb-4">

          <h6 class="text-uppercase font-weight-bold">

            <strong>Company name</strong>

          </h6>

          <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

          <p>Here you can use rows and columns here to organize your footer content. Lorem ipsum dolor sit amet,

            consectetur

            adipisicing elit.</p>

        </div>
        <!-- First column -->

        <!-- Second column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">

          <h6 class="text-uppercase font-weight-bold">

            <strong>Products</strong>

          </h6>

          <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

          <p>

            <a href="#!">MDBootstrap</a>

          </p>

          <p>

            <a href="#!">MDWordPress</a>

          </p>

          <p>

            <a href="#!">BrandFlow</a>

          </p>

          <p>

            <a href="#!">Bootstrap Angular</a>

          </p>

        </div>
        <!-- Second column -->

        <!-- Third column -->
        <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">

          <h6 class="text-uppercase font-weight-bold">

            <strong>Useful links</strong>

          </h6>

          <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

          <p>

            <a href="#!">Your Account</a>

          </p>

          <p>

            <a href="#!">Become an Affiliate</a>

          </p>

          <p>

            <a href="#!">Shipping Rates</a>

          </p>

          <p>

            <a href="#!">Help</a>

          </p>

        </div>
        <!-- Third column -->

        <!-- Fourth column -->
        <div class="col-md-4 col-lg-3 col-xl-3">

          <h6 class="text-uppercase font-weight-bold">

            <strong>Contact</strong>

          </h6>

          <hr class="blue mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">

          <p>

            <i class="fas fa-home mr-3"></i> New York, NY 10012, US</p>

          <p>

            <i class="fas fa-envelope mr-3"></i> info@example.com</p>

          <p>

            <i class="fas fa-phone mr-3"></i> + 01 234 567 88</p>

          <p>

            <i class="fas fa-print mr-3"></i> + 01 234 567 89</p>

        </div>
        <!-- Fourth column -->

      </div>

    </div>
    <!-- Footer Links -->

    <!-- Copyright -->
    <div class="footer-copyright py-3 text-center">

      <div class="container-fluid">

        © 2019 Copyright: <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> MDBootstrap.com </a>

      </div>

    </div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->

  <!-- Cart Modal -->
  <div class="modal fade cart-modal" id="cart-modal-ex" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">

    <div class="modal-dialog" role="document">

      <!-- Content -->
      <div class="modal-content">

        <!-- Header -->
        <div class="modal-header">

          <h4 class="modal-title font-weight-bold dark-grey-text" id="myModalLabel">Your cart</h4>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">

            <span aria-hidden="true">&times;</span>

          </button>

        </div>

        <!-- Body -->
        <div class="modal-body">

          <table class="table table-hover">

            <thead>

              <tr>

                <th>#</th>

                <th>Product name</th>

                <th>Price</th>

                <th>Remove</th>

              </tr>

            </thead>

            <tbody>

              <tr>

                <th scope="row">1</th>

                <td>Product 1</td>

                <td>100$</td>

                <td>

                  <a>

                    <i class="fas fa-eraser"></i>

                  </a>

                </td>

              </tr>

              <tr>

                <th scope="row">2</th>

                <td>Product 2</td>

                <td>100$</td>

                <td>

                  <a>

                    <i class="fas fa-eraser"></i>

                  </a>

                </td>

              </tr>

              <tr>

                <th scope="row">3</th>

                <td>Product 3</td>

                <td>100$</td>

                <td>

                  <a>

                    <i class="fas fa-eraser"></i>

                  </a>

                </td>

              </tr>

              <tr>

                <th scope="row">4</th>

                <td>Product 4</td>

                <td>100$</td>

                <td>

                  <a>

                    <i class="fas fa-eraser"></i>

                  </a>

                </td>

              </tr>

            </tbody>

          </table>

          <button class="btn btn-primary btn-rounded btn-sm">Checkout</button>

        </div>

        <!-- Footer -->
        <div class="modal-footer">

          <button type="button" class="btn blue darken-3 btn-rounded btn-sm" data-dismiss="modal">Close</button>

        </div>

      </div>
      <!-- Content -->

    </div>

  </div>
  <!-- Cart Modal -->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="<?php echo site_url('assets/js/jquery-3.3.1.min.js');?>"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="<?php echo site_url('assets/js/popper.min.js');?>"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="<?php echo site_url('assets/js/bootstrap.min.js');?>"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="<?php echo site_url('assets/js/mdb.min.js');?>"></script>
  <script type="text/javascript">
    /* WOW.js init */
    new WOW().init();

    // MDB Lightbox Init
    $(function () {
      $("#mdb-lightbox-ui").load("mdb-addons/mdb-lightbox-ui.html");
    });

    // Tooltips Initialization
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    // Material Select Initialization
    $(document).ready(function () {

      $('.mdb-select').material_select();
    });

    // SideNav Initialization
    $(".button-collapse").sideNav();

  </script>

</body>

</html>
