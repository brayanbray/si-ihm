create table administrateur(
    id int not null primary key auto_increment,
    login varchar(50),
    mdp varchar(40)
)ENGINE=InnoDB;

create table caisse(
    id int not null primary key auto_increment,
    nom varchar(50)
)ENGINE=InnoDB;

create table categorie(
    id int not null primary key auto_increment,
    nom varchar(10) 
)ENGINE=InnoDB;

create table produit(
    id int not null primary key auto_increment,
    nom varchar(50),
    prix float,
    categorie int,
    foreign key (categorie) references categorie(id)
)ENGINE=InnoDB;

create table panier(
    id int not null primary key auto_increment,
    produit int,
    caisse int,
    foreign key (produit) references produit(id),
    foreign key (caisse) references caisse(id)
)ENGINE=InnoDB;

create table panier_valider(
    id int not null primary key auto_increment,
    panier int,
    dateValidation date,
    foreign key (panier) references panier(id)
)ENGINE=InnoDB;

insert into administrateur values(1,'login1',sha1('mdp1'));
insert into administrateur values(2,'login2',sha1('mdp2'));
insert into administrateur values(3,'login3',sha1('mdp3'));

insert into caisse values(1,'caisse n*1');
insert into caisse values(2,'caisse n*2');
insert into caisse values(3,'caisse n*3');

insert into categorie values(1,'Nourriture');
insert into categorie values(2,'High-tech');
insert into categorie values(3,'Enfants');

insert into produit values(1,'Souris',3.5,2);
insert into produit values(2,'Clavier',4,2);
-- insert into produit values(3,'');